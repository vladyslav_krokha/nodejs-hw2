const express = require('express');
const router = express.Router();

const {
    registration,
    logIn
} = require('../services/authService');

router.post('/register', async (req, res) => {
    try {
        const {
            username,
            password
        } = req.body;

        if (!username | !password) throw '!user or !password'

        await registration({username, password});

        res.json({message: 'Success'});
    } catch (err) {
        res.status(400).json({message: err})    
    }
});

router.post('/login', async (req, res) => {
    try {
        const {
            username,
            password
        } = req.body;

        if (!username | !password) throw 400

        const jwt_token = await logIn({username, password});
        res.json({message: 'Success', jwt_token});
    } catch (err) {
        res.status(400).json({message: 'login error'});
    }
});

module.exports = {
    authRouter: router
}
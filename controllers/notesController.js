const express = require('express');
const router = express.Router();

const {
    getNotesByUserId,
    addNoteToUser
} = require('../services/notesService');

router.get('/', async (req, res) => {
    const { userId } = req.user;

    const notes = await getNotesByUserId(userId);

    res.json({notes});
});

router.post('/', async (req, res) => {
    const { userId } = req.user;

    await addNoteToUser(userId, req.body);

    res.json({message: "Success"});
});

module.exports = {
    notesRouter: router
}
const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken');

const {getUser, deleteUser, patchUser} = require('../services/userService')

router.get('/me', async (req, res) => {
	try {
		const {
			authorization
		} = req.headers;

		if (!authorization) throw 400

		const [, token] = authorization.split(' ');
		const tokenPayload = jwt.verify(token, 'secret');
		const username = tokenPayload.username
		const user = await getUser(username)

		res.json({
			user: {
				_id: user._id,
				uername: user.username,
				createdDate: user.createdDate
			}
		})
	} catch(err) {
		if (err == 400) res.status(400).json('bad request')
		else res.status(500).json(err.message)
	}
})

router.delete('/me', async (req, res) => {
	try {
		const {
			authorization
		} = req.headers;

		if (!authorization) throw 400

		const [, token] = authorization.split(' ');
		const tokenPayload = jwt.verify(token, 'secret');
		const username = tokenPayload.username
		
		res.json({
			message: await deleteUser(username)
		})
	} catch(err) {
		if (err == 400) res.status(400).json('bad request')
		else res.status(500).json(err.message)
	}
})

router.patch('/me', async (req, res) => {
	try {
		const {
			authorization
		} = req.headers;
		const newPassword = req.body.newPassword
		if (!authorization | !newPassword) throw 400

		const [, token] = authorization.split(' ');
		const tokenPayload = jwt.verify(token, 'secret');
		const username = tokenPayload.username
		
		res.json({
			message: await patchUser(username, newPassword)
		})
	} catch(err) {
		if (err == 400) res.status(400).json('bad request')
		else res.status(500).json(err.message)
	}
})

module.exports = {
	userRouter: router
}
require('dotenv').config()
const express = require('express');
const morgan = require('morgan')
const mongoose = require('mongoose');
const app = express();

const {authRouter} = require('./controllers/authController'); 
const {userRouter} = require('./controllers/usersController'); 
const {notesRouter} = require('./controllers/notesController'); 
const {authMiddleware} = require('./middlewares/authMiddleware'); 

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use(authMiddleware);
app.use('/api/users', userRouter);
app.use('/api/notes', notesRouter);

const start = async () => {
    try {
        await mongoose.connect('mongodb+srv://testuser:testpass@cluster0.lpyof.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {
            useNewUrlParser: true, useUnifiedTopology: true
        });
    
        app.listen(process.env.PORT);
    } catch (err) {
        console.error(`Error on server startup: ${err.message}`);
    }
}

start();
const {Note} = require('../models/noteModel');

const getNotesByUserId = async (userId) => {
    const notes = await Note.find({userId});
    return notes;
}

const addNoteToUser = async (userId, notePayload) => {
    const note = new Book({...notePayload, userId});
    await note.save();
}

module.exports = {
    getNotesByUserId,
    addNoteToUser
};
const bcrypt = require('bcrypt')
const {User} = require('../models/userModel');

const getUser = async (username) => {
	const user = await User.findOne({username})

	if (!user) {
			throw new Error('No users found');
	}

	return user
}

const deleteUser = async (username) => {
	const user = await User.findOne({username})

	if (!user) throw new Error('No users found');
	user.deleteOne()
	return 'Success'
}

const patchUser = async (username, password) => {
	const user = await User.findOne({username})

	if (!user) throw new Error('No users found');

	user.password = await bcrypt.hash(password, 10)
	user.update()
	return 'Success'
}

module.exports = {
	getUser,
	deleteUser,
	patchUser
}
const mongoose = require('mongoose')

const Note = mongoose.model('Note', {
	userId: {
		type: mongoose.Schema.Types.ObjectId,
		required: true
	},
	completed: Boolean,
	text: {
		type: String,
		required: true
	}
})

module.exports = { Note }